use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 6;

pub fn part1() -> i64 {
    let ipt_chars = get_input!().chars().collect_vec();
    for c in 0..ipt_chars.len() - 3 {
        let substr = &ipt_chars[c..c + 4];
        if substr.iter().unique().count() == 4 {
            return c as i64 + 4;
        }
    }
    0
}
pub fn part2() -> i64 {
    let ipt_chars = get_input!().chars().collect_vec();
    for c in 0..ipt_chars.len() - 13 {
        let substr = &ipt_chars[c..c + 14];
        if substr.iter().unique().count() == 14 {
            return c as i64 + 14;
        }
    }
    0
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 1275);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 3605);
    }
}
