use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 1;

pub fn part1() -> i64 {
    let input = get_input!();
    input
        .split("\n\n")
        .map(|elf| {
            elf.split('\n')
                .map(|food| food.parse::<i64>().unwrap_or(0))
                .sum()
        })
        .max()
        .unwrap()
}
pub fn part2() -> i64 {
    let input = get_input!();
    input
        .split("\n\n")
        .map(|elf| {
            elf.split('\n')
                .map(|food| food.parse::<i64>().unwrap_or(0))
                .sum::<i64>()
        })
        .sorted()
        .rev()
        .take(3)
        .sum()
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 67622);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 201491);
    }
}
