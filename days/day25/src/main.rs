use utils::*;

const CURRENT_DAY: u8 = 25;

fn parse(input: &str) -> i64 {
    input
        .chars()
        .rev()
        .enumerate()
        .map(|(i, c)| {
            let no = match c {
                '0' => 0,
                '1' => 1,
                '2' => 2,
                '-' => -1,
                '=' => -2,
                _ => panic!("Invalid char: {}", c),
            };
            no * 5_i64.pow(i as u32)
        })
        .sum()
}

pub fn to_snafu(mut no: i64) -> String {
    let mut s = String::new();

    while no != 0 {
        let digit = no % 5;
        no /= 5;
        let d = match digit {
            0 => "0",
            1 => "1",
            2 => "2",
            3 => {
                no += 1;
                "="
            }
            4 => {
                no += 1;
                "-"
            }
            _ => panic!("invalid"),
        };
        s += d;
    }
    s.chars().rev().collect()
}

pub fn part1() -> String {
    let input = read_input!("\n", String);
    to_snafu(input.iter().map(|x| parse(x)).sum())
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), "2=000=22-0-102=-1001");
    }
}
