use utils::*;

const CURRENT_DAY: u8 = 1;

pub fn part1() -> i64 {
    // let input = read_input!("\n", ",", i64);
    // print!("{:?}", input);
    0
}
pub fn part2() -> i64 {
    // let input = read_input!("\n", ",", i64);
    // print!("{:?}", input);
    0
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 0);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 0);
    }
}
