#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use utils::*;

const CURRENT_DAY: u8 = 15;

#[derive(Debug)]
pub struct Beacon {
    sensor_x: i64,
    sensor_y: i64,
    beacon_x: i64,
    beacon_y: i64,
    dist: i64,
}

impl Beacon {
    pub fn new(sensor_x: i64, sensor_y: i64, beacon_x: i64, beacon_y: i64) -> Self {
        Beacon {
            sensor_x,
            sensor_y,
            beacon_x,
            beacon_y,
            dist: (beacon_x - sensor_x).abs() + (beacon_y - sensor_y).abs(),
        }
    }
}

pub fn part1() -> i64 {
    let beacons = parser::BeaconsParser::new().parse(&get_input!()).unwrap();

    let row = 2000000;
    let mut count = 0;

    for col in -5_000_000..5_000_000 {
        if beacons.iter().any(|beacon| {
            let dist = (beacon.sensor_x - col).abs() + (beacon.sensor_y - row).abs();
            dist <= beacon.dist && (col != beacon.beacon_x || row != beacon.beacon_y)
        }) {
            count += 1;
        }
    }

    count
}

pub fn part2() -> i64 {
    let beacons = parser::BeaconsParser::new().parse(&get_input!()).unwrap();

    for beacon in &beacons {
        for i in 0..4_000_000 {
            let p1 = (beacon.sensor_x - (beacon.dist + 1) + i, beacon.sensor_y + i);
            let p2 = (beacon.sensor_x - (beacon.dist + 1) + i, beacon.sensor_y - i);
            let p3 = (beacon.sensor_x + (beacon.dist + 1) - i, beacon.sensor_y + i);
            let p4 = (beacon.sensor_x + (beacon.dist + 1) - i, beacon.sensor_y - i);

            for pos in [p1, p2, p3, p4] {
                if pos.0 < 0 || pos.1 < 0 || pos.0 > 4_000_000 || pos.1 > 4_000_000 {
                    continue;
                }

                if beacons.iter().all(|beacon| {
                    let dist = (beacon.sensor_x - pos.0).abs() + (beacon.sensor_y - pos.1).abs();
                    dist >= beacon.dist
                }) {
                    return pos.1 + pos.0 * 4_000_000;
                }
            }
        }
    }
    panic!("No solution found"); // too low 11066339135800
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 5607466);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 12543202766584);
    }
}
