#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use std::{cell::RefCell, collections::HashMap, rc::Rc};

use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 7;

#[derive(Debug, Clone)]
pub enum Command {
    Ls,
    Cd(String),
    CdRoot,
    CdParent,
}

#[derive(Debug, Clone)]
pub enum Content {
    File(String, usize),
    Directory(String, Vec<Content>),
}

type NodeHandle = Rc<RefCell<DirLayout>>;

#[derive(Debug, Clone, Default)]
struct DirLayout {
    pub files: HashMap<String, usize>,
    pub subdirs: HashMap<String, NodeHandle>,
    pub parent: Option<NodeHandle>,
}

fn all_dirs(n: NodeHandle) -> Box<dyn Iterator<Item = NodeHandle>> {
    let subdirs = n.borrow().subdirs.values().cloned().collect_vec();
    Box::new(std::iter::once(n.clone()).chain(subdirs.into_iter().flat_map(all_dirs)))
}

impl DirLayout {
    // pub fn new(dir: Option<NodeHandle>) -> Self {
    //     Self {
    //         files: HashMap::new(),
    //         subdirs: HashMap::new(),
    //         parent: dir,
    //     }
    // }

    pub fn get_size(&self) -> usize {
        self.files.values().sum::<usize>()
            + self
                .subdirs
                .values()
                .map(|d| d.borrow().get_size())
                .sum::<usize>()
    }

    // pub fn get_dirs_ct_smaller_than(&self, size: usize) -> usize {
    //     let mut res = 0;

    //     println!("{} {}", self.get_size(), size);

    //     if self.get_size() <= size {
    //         res += 1;
    //     }
    //     res += self
    //         .subdirs
    //         .iter()
    //         // .values()
    //         .map(|(x, d)| {
    //             println!("{}:{}", x, d.borrow().get_dirs_ct_smaller_than(size));
    //             d.borrow().get_dirs_ct_smaller_than(size)
    //         })
    //         .sum::<usize>();
    //     res
    // }
}

#[derive(Debug, Clone)]
pub enum Entry {
    Command(Command),
    Content(Content),
}

pub type History = Vec<Entry>;

fn load_tree(input: Vec<Entry>) -> NodeHandle {
    let root = Rc::new(RefCell::new(DirLayout::default()));
    let mut curr_dir = root.clone();

    for entry in input {
        match entry {
            Entry::Command(cmd) => match cmd {
                Command::Ls => {}
                Command::Cd(path) => {
                    let entry = curr_dir
                        .borrow_mut()
                        .subdirs
                        .entry(path)
                        .or_default()
                        .clone();
                    entry.borrow_mut().parent = Some(curr_dir.clone());
                    curr_dir = entry;
                }
                Command::CdParent => {
                    let parent = curr_dir.borrow().parent.clone().unwrap();
                    curr_dir = parent;
                }
                Command::CdRoot => {}
            },
            Entry::Content(content) => match content {
                Content::Directory(name, _) => {
                    let entry = curr_dir
                        .borrow_mut()
                        .subdirs
                        .entry(name)
                        .or_default()
                        .clone();
                    entry.borrow_mut().parent = Some(curr_dir.clone());
                }
                Content::File(name, size) => {
                    curr_dir.borrow_mut().files.entry(name).or_insert(size);
                }
            },
        }
    }
    root
}

pub fn part1() -> i64 {
    let input = parser::HistoryParser::new().parse(&get_input!()).unwrap();
    let root = load_tree(input);
    all_dirs(root)
        .map(|d| d.borrow().get_size())
        .filter(|s| s <= &100_000)
        .sum::<usize>() as i64
}

pub fn part2() -> i64 {
    let input = parser::HistoryParser::new().parse(&get_input!()).unwrap();
    let root = load_tree(input);

    let size = root.borrow().get_size();
    let neede_size = size - 40_000_000;

    all_dirs(root)
        .map(|d| d.borrow().get_size())
        .filter(|s| s >= &neede_size)
        .min()
        .unwrap() as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 1743217);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 8319096);
    }
}
