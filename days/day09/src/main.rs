#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use std::collections::HashSet;

use utils::*;

const CURRENT_DAY: u8 = 9;

#[derive(Debug)]
pub enum Moves {
    Up(i64),
    Down(i64),
    Left(i64),
    Right(i64),
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
struct Coord {
    x: i64,
    y: i64,
}
impl Coord {
    fn new() -> Coord {
        Coord { x: 0, y: 0 }
    }
}

impl Default for Coord {
    fn default() -> Coord {
        Coord::new()
    }
}

#[allow(dead_code)]
fn print_grid(head: &Coord, tails: &[Coord]) {
    let size = 30i64;
    let mut grid = vec![vec!['.'; size as usize]; size as usize];
    grid[size as usize / 2][size as usize / 2] = 'S';
    for (t, tail) in tails.iter().enumerate().rev() {
        grid[(size / 2 + tail.y) as usize][(size / 2 + tail.x) as usize] = (t as u8 + b'1') as char;
    }
    grid[(size / 2 + head.y) as usize][(size / 2 + head.x) as usize] = 'H';
    for row in grid {
        for col in row {
            print!("{}", col);
        }
        println!();
    }
    println!();
    println!();
}

fn adjust_tail_pos(head_pos: &Coord, tail_pos: &mut Coord) {
    let x_diff = head_pos.x.abs_diff(tail_pos.x);
    let y_diff = head_pos.y.abs_diff(tail_pos.y);

    if !(x_diff != 1 && x_diff != 0 || y_diff != 0 && y_diff != 1) {
        return;
    }

    let mut new_tail_pos = head_pos.clone();
    if head_pos.x > tail_pos.x + 1 {
        new_tail_pos.x -= 1;
    }
    if head_pos.x < tail_pos.x - 1 {
        new_tail_pos.x += 1;
    }
    if head_pos.y > tail_pos.y + 1 {
        new_tail_pos.y -= 1;
    }
    if head_pos.y < tail_pos.y - 1 {
        new_tail_pos.y += 1;
    }
    tail_pos.x = new_tail_pos.x;
    tail_pos.y = new_tail_pos.y;
}

pub fn part1() -> i64 {
    let input = parser::MovesParser::new().parse(&get_input!()).unwrap();

    let mut tail_visits = HashSet::new();

    let mut head_pos = Coord::new();
    let mut tail_pos = Coord::new();
    tail_visits.insert(tail_pos.clone());

    for mv in input {
        let (dir, ct) = match mv {
            Moves::Up(n) => (Coord { x: 0, y: 1 }, n),
            Moves::Down(n) => (Coord { x: 0, y: -1 }, n),
            Moves::Left(n) => (Coord { x: -1, y: 0 }, n),
            Moves::Right(n) => (Coord { x: 1, y: 0 }, n),
        };
        for _ in 0..ct {
            head_pos.x += dir.x;
            head_pos.y += dir.y;

            adjust_tail_pos(&head_pos, &mut tail_pos);
            tail_visits.insert(tail_pos.clone());
        }
    }
    tail_visits.len() as i64
}

pub fn part2() -> i64 {
    let input = parser::MovesParser::new().parse(&get_input!()).unwrap();

    let mut tail_visits = HashSet::new();

    let mut head_pos = Coord::new();
    let mut tail_poses = vec![Coord::new(); 9];
    tail_visits.insert(tail_poses.last().unwrap().clone());

    for mv in input {
        let (dir, ct) = match mv {
            Moves::Up(n) => (Coord { x: 0, y: 1 }, n),
            Moves::Down(n) => (Coord { x: 0, y: -1 }, n),
            Moves::Left(n) => (Coord { x: -1, y: 0 }, n),
            Moves::Right(n) => (Coord { x: 1, y: 0 }, n),
        };
        for _ in 0..ct {
            head_pos.x += dir.x;
            head_pos.y += dir.y;

            let mut loc_head = head_pos.clone();
            let mut new_tail_poses = vec![];

            for t in tail_poses.iter() {
                let mut loc_tail = t.clone();
                adjust_tail_pos(&loc_head, &mut loc_tail);
                loc_head = loc_tail.clone();
                new_tail_poses.push(loc_tail.clone());
            }
            tail_poses = new_tail_poses;

            tail_visits.insert(tail_poses.last().unwrap().clone());
        }
    }
    tail_visits.len() as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 6503);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 2724);
    }
}
