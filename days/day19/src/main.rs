use utils::*;

const CURRENT_DAY: u8 = 19;

#[derive(Debug, Clone, Copy)]
struct Blueprint {
    ore_robot_cost: u8,
    clay_robot_cost: u8,
    obsidian_robot_cost: (u8, u8),
    geode_robot_cost: (u8, u8),
}

#[derive(Debug, Clone, Copy)]
struct State {
    ore: u8,
    ore_bot_count: u8,
    clay: u8,
    clay_bot_count: u8,
    obsidian: u8,
    obsidian_bot_count: u8,
    geode: u8,
    geode_bot_count: u8,
}

impl State {
    fn new() -> Self {
        Self {
            ore: 0,
            ore_bot_count: 1,
            clay: 0,
            clay_bot_count: 0,
            obsidian: 0,
            obsidian_bot_count: 0,
            geode: 0,
            geode_bot_count: 0,
        }
    }
}

fn do_step(blueprint: &Blueprint, mut state: State, steps_todo: u8) -> u64 {
    let mut res = vec![];

    if steps_todo == 0 {
        return state.geode as u64;
    }
    state.ore += state.ore_bot_count;
    state.clay += state.clay_bot_count;
    state.obsidian += state.obsidian_bot_count;
    state.geode += state.geode_bot_count;

    if state.ore >= blueprint.ore_robot_cost {
        let mut ore_state = state;
        ore_state.ore -= blueprint.ore_robot_cost;
        ore_state.ore_bot_count += 1;
        res.push(do_step(blueprint, ore_state, steps_todo - 1));
    }
    if state.ore >= blueprint.clay_robot_cost {
        let mut clay_state = state;
        clay_state.ore -= blueprint.clay_robot_cost;
        clay_state.clay_bot_count += 1;
        res.push(do_step(blueprint, clay_state, steps_todo - 1));
    }
    if state.ore >= blueprint.obsidian_robot_cost.0 && state.clay >= blueprint.obsidian_robot_cost.1
    {
        let mut obsidian_state = state;
        obsidian_state.ore -= blueprint.clay_robot_cost;
        obsidian_state.obsidian_bot_count += 1;
        res.push(do_step(blueprint, obsidian_state, steps_todo - 1));
    }
    if state.ore >= blueprint.geode_robot_cost.0 && state.clay >= blueprint.geode_robot_cost.1 {
        let mut geode_state = state;
        geode_state.ore -= blueprint.clay_robot_cost;
        geode_state.geode_bot_count += 1;
        res.push(do_step(blueprint, geode_state, steps_todo - 1));
    }

    res.push(do_step(blueprint, state, steps_todo - 1));

    *res.iter().max().unwrap()
}

pub fn part1() -> i64 {
    let input = read_input!("\n", " ", String);
    let blueprints = input
        .iter()
        .map(|x| {
            let ore_robot_cost = x[6].parse().unwrap();
            let clay_robot_cost = x[12].parse().unwrap();
            let obsidian_robot_cost = (x[18].parse().unwrap(), x[21].parse().unwrap());
            let geode_robot_cost = (x[27].parse().unwrap(), x[30].parse().unwrap());
            Blueprint {
                ore_robot_cost,
                clay_robot_cost,
                obsidian_robot_cost,
                geode_robot_cost,
            }
        })
        .collect::<Vec<_>>();

    blueprints
        .iter()
        .enumerate()
        .map(|(i, bp)| do_step(bp, State::new(), 24) as i64 * i as i64)
        .sum::<i64>()
}
pub fn part2() -> i64 {
    let input = read_input!("\n", " ", String);
    print!("{:?}", input);
    0
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {

    #[test]
    fn test_part1() {
        // assert_eq!(part1(), 0);
    }

    #[test]
    fn test_part2() {
        // assert_eq!(part2(), 0);
    }
}
