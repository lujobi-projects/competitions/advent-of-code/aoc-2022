#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use std::fmt::Debug;

use utils::*;

const CURRENT_DAY: u8 = 13;

#[derive(Clone, PartialEq, Eq)]
pub enum Entry {
    Number(usize),
    List(Vec<Entry>),
}

impl Debug for Entry {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Entry::Number(n) => write!(f, "{}", n),
            Entry::List(l) => write!(f, "{:?}", l),
        }
    }
}

fn is_larger_than(left: &Entry, right: &Entry) -> std::cmp::Ordering {
    match (left, right) {
        (Entry::Number(l), Entry::Number(r)) => l.cmp(r),
        (Entry::Number(l), Entry::List(_)) => {
            is_larger_than(&Entry::List(vec![Entry::Number(*l)]), right)
        }
        (Entry::List(_), Entry::Number(r)) => {
            is_larger_than(left, &Entry::List(vec![Entry::Number(*r)]))
        }
        (Entry::List(l), Entry::List(r)) => {
            let mut li = l.iter();
            let mut ri = r.iter();
            match (li.next(), ri.next()) {
                (Some(l), Some(r)) => {
                    let cmp = is_larger_than(l, r);
                    if cmp == std::cmp::Ordering::Equal {
                        is_larger_than(
                            &Entry::List(li.cloned().collect()),
                            &Entry::List(ri.cloned().collect()),
                        )
                    } else {
                        cmp
                    }
                }
                (Some(_), None) => std::cmp::Ordering::Greater,
                (None, Some(_)) => std::cmp::Ordering::Less,
                (None, None) => std::cmp::Ordering::Equal,
            }
        }
    }
}

pub fn part1() -> i64 {
    let input: Vec<Entry> = parser::EntriesParser::new().parse(&get_input!()).unwrap();

    input
        .chunks(2)
        .enumerate()
        .filter(|(_, pair)| {
            let [upper, lower] = pair else {
                panic!("Invalid")
            };
            is_larger_than(upper, lower) != std::cmp::Ordering::Greater
        })
        .map(|(i, _)| i as i64 + 1)
        .sum()
}

pub fn part2() -> i64 {
    let mut input: Vec<Entry> = parser::EntriesParser::new().parse(&get_input!()).unwrap();
    let e1 = Entry::List(vec![Entry::List(vec![Entry::Number(2)])]);
    input.push(e1.clone());
    let e2 = Entry::List(vec![Entry::List(vec![Entry::Number(6)])]);
    input.push(e2.clone());
    input.sort_by(is_larger_than);
    (input.iter().position(|x| x == &e1).unwrap() as i64 + 1)
        * (input.iter().position(|x| x == &e2).unwrap() as i64 + 1)
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 6656);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 19716);
    }
}
