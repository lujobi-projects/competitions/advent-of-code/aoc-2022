use std::str::FromStr;
use utils::*;

const CURRENT_DAY: u8 = 8;

pub fn part1() -> i64 {
    let input = read_input!("\n", String);
    let trees = input
        .iter()
        .map(|line| {
            line.chars()
                .map(|t| u8::from_str(format!("{}", t).as_str()).unwrap())
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();

    let outer = 2 * (trees.len() + trees[0].len() - 2) as i64;
    let mut visible = 0;

    for i in 1..trees.len() - 1 {
        for j in 1..trees[i].len() - 1 {
            let height = trees[i][j];
            if trees[i][0..j].iter().all(|h| h < &height)
                || trees[i][j + 1..trees[i].len()].iter().all(|h| h < &height)
                || trees[0..i].iter().all(|row| row[j] < height)
                || trees[i + 1..trees.len()].iter().all(|row| row[j] < height)
            {
                visible += 1;
            }
        }
    }

    visible + outer
}
pub fn part2() -> i64 {
    let input = read_input!("\n", String);
    let trees = input
        .iter()
        .map(|line| {
            line.chars()
                .map(|t| u8::from_str(format!("{}", t).as_str()).unwrap())
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();

    let mut best_score = 0;

    for i in 1..trees.len() - 1 {
        for j in 1..trees[i].len() - 1 {
            let height = trees[i][j];
            let mut left_score = 0;
            for t in trees[i][0..j].iter().rev() {
                if t < &height {
                    left_score += 1;
                } else {
                    left_score += 1;
                    break;
                }
            }
            let mut right_score = 0;
            for t in trees[i][j + 1..trees[i].len()].iter() {
                if t < &height {
                    right_score += 1;
                } else {
                    right_score += 1;
                    break;
                }
            }
            let mut up_score = 0;
            for row in trees[0..i].iter().rev() {
                if row[j] < height {
                    up_score += 1;
                } else {
                    up_score += 1;
                    break;
                }
            }
            let mut down_score = 0;
            for row in trees[i + 1..trees.len()].iter() {
                if row[j] < height {
                    down_score += 1;
                } else {
                    down_score += 1;
                    break;
                }
            }
            best_score = best_score.max(left_score * right_score * up_score * down_score);
        }
    }
    best_score // 1501440 too high
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 1684);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 486540);
    }
}
