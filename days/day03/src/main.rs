use std::collections::HashSet;

use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 3;

pub fn part1() -> i64 {
    let input = read_input!("\n", String);
    input
        .iter()
        .map(|x| {
            let x_len = x.len();
            let _upper = x[..x_len / 2].chars();
            let lower = x[x_len / 2..].to_string();

            let mut upper: HashSet<char> = HashSet::new();
            for c in _upper {
                upper.insert(c);
            }

            let mut sum = 0;

            for u in upper {
                if lower.contains(u) {
                    if u.is_uppercase() {
                        sum += (u as i64 - 'A' as i64) + 27;
                    } else {
                        sum += (u as i64 - 'a' as i64) + 1;
                    }
                }
            }
            sum
        })
        .sum()
}
pub fn part2() -> i64 {
    let input = read_input!("\n", String);

    input
        .iter()
        .chunks(3)
        .into_iter()
        .map(|x| {
            let x_vec = x.into_iter().collect_vec();
            let [a, b, c] = x_vec.as_slice() else {panic!("Invalid")};

            let mut upper: HashSet<char> = HashSet::new();
            for _c in a.chars() {
                upper.insert(_c);
            }

            let mut sum = 0;

            for u in upper {
                if b.contains(u) && c.contains(u) {
                    sum += if u.is_uppercase() {
                        (u as i64 - 'A' as i64) + 27
                    } else {
                        (u as i64 - 'a' as i64) + 1
                    }
                }
            }
            sum
        })
        .sum()
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 7990);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 2602);
    }
}
