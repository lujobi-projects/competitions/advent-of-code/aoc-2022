use std::str::FromStr;
use utils::*;

const CURRENT_DAY: u8 = 14;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Fill {
    Air,
    Sand,
    Rock,
}

type Grid = Vec<Vec<Fill>>;
type Coord = (usize, usize);

fn fill_line(gi: &mut Grid, c1: Coord, c2: Coord) {
    let (x1, y1) = c1;
    let (x2, y2) = c2;
    if x1 == x2 {
        for item in gi.iter_mut().take(y1.max(y2) + 1).skip(y1.min(y2)) {
            item[x1] = Fill::Rock;
        }
    } else if y1 == y2 {
        for x in x1.min(x2)..x1.max(x2) + 1 {
            gi[y1][x] = Fill::Rock;
        }
    } else {
        panic!("Invalid");
    }
}

fn fall_sand(gi: &mut Grid, c: Coord, stop: Option<usize>) -> bool {
    let (mut x, mut y) = c;
    loop {
        if stop.is_none() && y >= 499 {
            return false;
        } else if let Some(s) = stop {
            if y == s - 1 {
                break;
            }
        }
        if gi[y + 1][x] == Fill::Air {
            y += 1;
        } else if gi[y + 1][x - 1] == Fill::Air {
            x -= 1;
        } else if gi[y + 1][x + 1] == Fill::Air {
            x += 1;
        } else {
            break;
        }
    }
    gi[y][x] = Fill::Sand;
    true
}

fn parse(input: Vec<String>) -> Vec<Vec<Fill>> {
    let coords = input.iter().map(|row| {
        row.split(" -> ")
            .map(|pair| {
                let splitted = pair.split(",").collect::<Vec<_>>();
                let [x, y] = splitted.as_slice() else {
                    panic!("Invalid")
                };
                (usize::from_str(x).unwrap(), usize::from_str(y).unwrap())
            })
            .collect::<Vec<_>>()
    });

    let mut grid = vec![vec![Fill::Air; 1000]; 501];

    for line in coords {
        for i in 0..line.len() - 1 {
            fill_line(&mut grid, line[i], line[i + 1]);
        }
    }
    grid
}

pub fn part1() -> i64 {
    let input = read_input!("\n", String);

    let mut grid = parse(input);
    let mut count = 0;
    let sand_in = (500, 0);

    loop {
        if !fall_sand(&mut grid, sand_in, None) {
            break;
        }
        count += 1;
    }
    count
}

pub fn part2() -> i64 {
    let input = read_input!("\n", String);

    let mut grid = parse(input);
    let mut count = 0;
    let sand_in = (500, 0);

    let mut max_y = 0;
    for (y, item) in grid.iter().enumerate() {
        if item.iter().any(|&x| x == Fill::Rock) {
            max_y = y;
        }
    }

    loop {
        fall_sand(&mut grid, sand_in, Some(max_y + 2));
        count += 1;
        if grid[sand_in.1][sand_in.0] == Fill::Sand {
            break;
        }
    }

    count
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 799);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 29076);
    }
}
