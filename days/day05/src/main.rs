use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 5;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Instruction {
    count: usize,
    start: usize,
    end: usize,
}

fn parse_ipt(ipt: Vec<String>) -> (Vec<Vec<char>>, Vec<Instruction>) {
    let mut stacks = vec![vec![]; 9];
    for l in ipt[0..8].iter().rev() {
        let chars = l.chars().collect_vec();
        for i in 0..9 {
            let c = chars[1 + 4 * i];
            if c != ' ' {
                stacks[i].push(c);
            }
        }
    }

    let mut instructions = vec![];

    for l in ipt[10..].iter() {
        let splitline = l.split(' ').collect::<Vec<_>>();
        instructions.push(Instruction {
            count: splitline[1].parse::<usize>().unwrap(),
            start: splitline[3].parse::<usize>().unwrap() - 1,
            end: splitline[5].parse::<usize>().unwrap() - 1,
        })
    }

    (stacks, instructions)
}

pub fn part1() -> String {
    let input = read_input!("\n", String);

    let (mut start, insns) = parse_ipt(input);

    for i in insns {
        let count = i.count;
        for _ in 0..count {
            let c = start[i.start].pop().unwrap();
            start[i.end].push(c);
        }
    }
    start.iter().map(|s| s.last().unwrap()).collect()
}
pub fn part2() -> String {
    let input = read_input!("\n", String);

    let (mut start, insns) = parse_ipt(input);

    for i in insns {
        let count = i.count;
        let mut stack = vec![];
        for _ in 0..count {
            stack.push(start[i.start].pop().unwrap());
        }
        stack.reverse();
        start[i.end].append(&mut stack);
    }

    start.iter().map(|s| s.last().unwrap()).collect()
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), "WSFTMRHPP");
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), "GSLCMFBRP");
    }
}
