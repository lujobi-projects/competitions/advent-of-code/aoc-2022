use std::{
    env,
    fs::{self, read_to_string, File},
    io::{self, Write},
    path::Path,
};

fn copy_dir_all(src: impl AsRef<Path>, dst: impl AsRef<Path>) -> io::Result<()> {
    fs::create_dir_all(&dst)?;
    for entry in fs::read_dir(src)? {
        let entry = entry?;
        let ty = entry.file_type()?;
        if ty.is_dir() {
            copy_dir_all(entry.path(), dst.as_ref().join(entry.file_name()))?;
        } else {
            fs::copy(entry.path(), dst.as_ref().join(entry.file_name()))?;
        }
    }
    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let arglen = args.len();
    if arglen == 2 || arglen == 3 {
        let day = args[1].parse::<u8>().unwrap();
        let day_str = format!("day{:0>2}", day);
        let new_day_path = Path::new("days").join(&day_str);

        // copy the directory .template/simple to a y directory
        let template_path = Path::new(".template").join(if arglen == 2 || args[2] == "simple" {
            "simple"
        } else if args[2] == "parsed" {
            "parsed"
        } else {
            panic!("Unknown template type")
        });

        // create the directory abort if exists
        _ = std::fs::create_dir(Path::new("days"));
        std::fs::create_dir(&new_day_path).unwrap();
        copy_dir_all(&template_path, &new_day_path).unwrap();

        // replace the day number in the newly created main.rs file and the cargo.toml file
        let main_path = new_day_path.join("src").join("main.rs");
        let mut main_file_content = read_to_string(&main_path).unwrap();
        main_file_content = main_file_content.replace(
            "const CURRENT_DAY: u8 = 1;",
            format!("const CURRENT_DAY: u8 = {};", day).as_str(),
        );
        let mut main_file = File::create(main_path).unwrap();
        main_file.write_all(main_file_content.as_bytes()).unwrap();

        let cargo_path = new_day_path.join("Cargo.toml");
        let mut cargo_file_content = read_to_string(&cargo_path).unwrap();
        cargo_file_content = cargo_file_content
            .replace(
                "name = \"template_parsed\"",
                format!("name = \"{}\"", day_str).as_str(),
            )
            .replace(
                "name = \"template_simple\"",
                format!("name = \"{}\"", day_str).as_str(),
            );
        let mut cargo_file = File::create(cargo_path).unwrap();
        cargo_file.write_all(cargo_file_content.as_bytes()).unwrap();

        // append days/dayX to Cargo.toml as workspace members entry if not already done
        let path = Path::new("Cargo.toml");
        let mut content = read_to_string(path).expect("failed to read file");
        if !content.contains(&format!("\"days/{}\"", day_str)) {
            // append ",\ndays/dayX" to the second to last line in content

            // find the second to last line
            let mut lines = content
                .lines()
                .map(|x| x.to_string())
                .collect::<Vec<String>>();
            let second_to_last_line_ind = lines.len() - 2;
            // replace the second to last line with the new line
            lines[second_to_last_line_ind] = format!(
                "{},\n\t\"days/{}\"",
                lines[second_to_last_line_ind], day_str
            );
            // join the lines back together
            content = lines.join("\n");

            let mut file = File::create(path).expect("failed to create file");
            file.write_all(content.as_bytes())
                .expect("failed to write file");
        }
    } else {
        println!("Usage: cargo run --bin newday <NUMBER> [simple|parsed]");
    }
}
