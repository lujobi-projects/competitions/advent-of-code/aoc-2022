use std::collections::HashSet;

use utils::*;

const CURRENT_DAY: u8 = 18;

const GRID_SIZE: i8 = 20;

type Coord3D = (i8, i8, i8);

pub fn part1() -> i64 {
    let input = read_input!("\n", ",", i8);
    let coords = input
        .iter()
        .map(|line| {
            let [x, y, z] = line.as_slice() else {
                panic!("Invalid input");
            };
            (*x, *y, *z)
        })
        .collect::<HashSet<Coord3D>>();

    let mut count = 0;

    for x in 0..GRID_SIZE {
        for y in 0..GRID_SIZE {
            for z in 0..GRID_SIZE {
                let coord = (x, y, z);
                if !coords.contains(&coord) {
                    continue;
                }
                if !coords.contains(&(x + 1, y, z)) {
                    count += 1;
                }
                if !coords.contains(&(x - 1, y, z)) {
                    count += 1;
                }
                if !coords.contains(&(x, y + 1, z)) {
                    count += 1;
                }
                if !coords.contains(&(x, y - 1, z)) {
                    count += 1;
                }
                if !coords.contains(&(x, y, z + 1)) {
                    count += 1;
                }
                if !coords.contains(&(x, y, z - 1)) {
                    count += 1;
                }
            }
        }
    }
    count
}
pub fn part2() -> i64 {
    let input = read_input!("\n", ",", i8);
    let coords = input
        .iter()
        .map(|line| {
            let [x, y, z] = line.as_slice() else {
                panic!("Invalid input");
            };
            (*x, *y, *z)
        })
        .collect::<HashSet<Coord3D>>();

    let mut outside = HashSet::new();

    let mut open_list = Vec::new();
    open_list.push((0, 0, 0));

    while let Some(coord) = open_list.pop() {
        if outside.contains(&coord) || coords.contains(&coord) {
            continue;
        }
        outside.insert(coord);
        if coord.0 <= GRID_SIZE {
            open_list.push((coord.0 + 1, coord.1, coord.2))
        }
        if coord.0 >= 0 {
            open_list.push((coord.0 - 1, coord.1, coord.2));
        }
        if coord.1 <= GRID_SIZE {
            open_list.push((coord.0, coord.1 + 1, coord.2));
        }
        if coord.1 >= 0 {
            open_list.push((coord.0, coord.1 - 1, coord.2));
        }
        if coord.2 <= GRID_SIZE {
            open_list.push((coord.0, coord.1, coord.2 + 1));
        }
        if coord.2 >= 0 {
            open_list.push((coord.0, coord.1, coord.2 - 1));
        }
    }

    let mut count = 0;

    for x in 0..GRID_SIZE {
        for y in 0..GRID_SIZE {
            for z in 0..GRID_SIZE {
                let coord = (x, y, z);
                if !coords.contains(&coord) {
                    continue;
                }
                if !coords.contains(&(x + 1, y, z)) && outside.contains(&(x + 1, y, z)) {
                    count += 1;
                }
                if !coords.contains(&(x - 1, y, z)) && outside.contains(&(x - 1, y, z)) {
                    count += 1;
                }
                if !coords.contains(&(x, y + 1, z)) && outside.contains(&(x, y + 1, z)) {
                    count += 1;
                }
                if !coords.contains(&(x, y - 1, z)) && outside.contains(&(x, y - 1, z)) {
                    count += 1;
                }
                if !coords.contains(&(x, y, z + 1)) && outside.contains(&(x, y, z + 1)) {
                    count += 1;
                }
                if !coords.contains(&(x, y, z - 1)) && outside.contains(&(x, y, z - 1)) {
                    count += 1;
                }
            }
        }
    }
    //1978 is too low
    count
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 3470);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 1986);
    }
}
