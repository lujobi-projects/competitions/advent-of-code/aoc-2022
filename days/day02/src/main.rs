use utils::*;

const CURRENT_DAY: u8 = 2;

enum Turn {
    Rock,
    Paper,
    Scissors,
}

enum Task {
    Win,
    Draw,
    Lose,
}

pub fn part1() -> i64 {
    let input = read_input!("\n", " ", char);
    input
        .iter()
        .map(|round| {
            let elf_turn = match round[0] {
                'A' => Turn::Rock,
                'B' => Turn::Paper,
                'C' => Turn::Scissors,
                _ => panic!("Invalid"),
            };
            let player_turn = match round[1] {
                'X' => Turn::Rock,
                'Y' => Turn::Paper,
                'Z' => Turn::Scissors,
                _ => panic!("Invalid"),
            };
            let pts = match elf_turn {
                Turn::Rock => match player_turn {
                    Turn::Rock => 3,
                    Turn::Paper => 6,
                    Turn::Scissors => 0,
                },
                Turn::Paper => match player_turn {
                    Turn::Rock => 0,
                    Turn::Paper => 3,
                    Turn::Scissors => 6,
                },
                Turn::Scissors => match player_turn {
                    Turn::Rock => 6,
                    Turn::Paper => 0,
                    Turn::Scissors => 3,
                },
            };

            let turn_pts = match player_turn {
                Turn::Rock => 1,
                Turn::Paper => 2,
                Turn::Scissors => 3,
            };
            pts + turn_pts
        })
        .sum()
}
pub fn part2() -> i64 {
    let input = read_input!("\n", " ", char);
    input
        .iter()
        .map(|round| {
            let elf_turn = match round[0] {
                'A' => Turn::Rock,
                'B' => Turn::Paper,
                'C' => Turn::Scissors,
                _ => panic!("Invalid"),
            };
            let player_turn = match round[1] {
                'X' => Task::Lose,
                'Y' => Task::Draw,
                'Z' => Task::Win,
                _ => panic!("Invalid"),
            };
            let pts = match elf_turn {
                Turn::Rock => match player_turn {
                    Task::Win => 2,
                    Task::Draw => 1,
                    Task::Lose => 3,
                },
                Turn::Paper => match player_turn {
                    Task::Win => 3,
                    Task::Draw => 2,
                    Task::Lose => 1,
                },
                Turn::Scissors => match player_turn {
                    Task::Win => 1,
                    Task::Draw => 3,
                    Task::Lose => 2,
                },
            };

            let turn_pts = match player_turn {
                Task::Win => 6,
                Task::Draw => 3,
                Task::Lose => 0,
            };
            pts + turn_pts
        })
        .sum() // 15728 too high
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 8392);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 10116);
    }
}
