#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use std::collections::HashMap;

use utils::*;

const CURRENT_DAY: u8 = 21;

#[derive(Debug, Clone)]
pub enum Operation {
    Add(String, String),
    Sub(String, String),
    Mul(String, String),
    Div(String, String),
    Yell(i64),
}
#[derive(Debug, Clone)]
pub struct Monkey {
    name: String,
    operation: Operation,
}

fn run_monkey(monkey_map: &mut HashMap<String, Operation>, name: &String) -> i64 {
    let monkey_op = monkey_map.get(name).unwrap();

    let res = match monkey_op.clone() {
        Operation::Yell(n) => n,
        Operation::Add(a, b) => run_monkey(monkey_map, &a) + run_monkey(monkey_map, &b),
        Operation::Sub(a, b) => run_monkey(monkey_map, &a) - run_monkey(monkey_map, &b),
        Operation::Mul(a, b) => run_monkey(monkey_map, &a) * run_monkey(monkey_map, &b),
        Operation::Div(a, b) => run_monkey(monkey_map, &a) / run_monkey(monkey_map, &b),
    };

    monkey_map.insert(name.clone(), Operation::Yell(res));
    res
}

pub fn part1() -> i64 {
    let input = parser::MonkeysParser::new().parse(&get_input!()).unwrap();
    let mut monkey_map = input
        .iter()
        .map(|m| (m.name.clone(), m.operation.clone()))
        .collect::<HashMap<String, Operation>>();
    run_monkey(&mut monkey_map, &String::from("root"))
}

pub fn part2() -> i64 {
    let input = parser::MonkeysParser::new().parse(&get_input!()).unwrap();
    let monkey_map = input
        .iter()
        .map(|m| (m.name.clone(), m.operation.clone()))
        .collect::<HashMap<String, Operation>>();

    let endpoints = match monkey_map.get("root").unwrap().clone() {
        Operation::Yell(_) => panic!("Should not happen"),
        Operation::Add(a, b) => (a, b),
        Operation::Sub(a, b) => (a, b),
        Operation::Mul(a, b) => (a, b),
        Operation::Div(a, b) => (a, b),
    };

    for human in 0..20_000 {
        let mut loc_monkey_map = monkey_map.clone();
        loc_monkey_map.insert(String::from("humn"), Operation::Yell(human));
        let a = run_monkey(&mut loc_monkey_map, &endpoints.0);
        let b = run_monkey(&mut loc_monkey_map, &endpoints.1);
        if a == b {
            return human;
        }
    }
    panic!("No solution found for part 2");
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        // assert_eq!(part1(), 0);
    }

    #[test]
    fn test_part2() {
        // assert_eq!(part2(), 0);
    }
}
