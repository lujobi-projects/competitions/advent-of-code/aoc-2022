use std::collections::HashMap;

use utils::*;

const CURRENT_DAY: u8 = 17;

#[derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
enum Tile {
    Bar,
    Plus,
    L,
    I,
    Box,
}
const TILE_ORDER: [Tile; 5] = [Tile::Bar, Tile::Plus, Tile::L, Tile::I, Tile::Box];
type TileMap = HashMap<Tile, Vec<(i32, i32)>>;
type Playfield = Vec<Vec<bool>>;

#[derive(Debug, Hash, Eq, PartialEq, Clone)]
struct GameState {
    playfield: Playfield,
    current_tile: Tile,
    current_wind_direction: char,
}

fn generate_tile_map() -> TileMap {
    let mut tile_map = HashMap::new();
    tile_map.insert(Tile::Bar, vec![(0, 0), (1, 0), (2, 0), (3, 0)]);
    tile_map.insert(Tile::Plus, vec![(1, 0), (0, 1), (1, 1), (2, 1), (1, 2)]);
    tile_map.insert(Tile::L, vec![(0, 0), (1, 0), (2, 0), (2, 1), (2, 2)]);
    tile_map.insert(Tile::I, vec![(0, 0), (0, 1), (0, 2), (0, 3)]);
    tile_map.insert(Tile::Box, vec![(0, 0), (1, 0), (0, 1), (1, 1)]);
    tile_map
}

fn find_max_y(playfield: &Playfield) -> i32 {
    let mut y_pos = 0;
    for (i, line) in playfield.iter().enumerate() {
        if line.iter().any(|x| *x) {
            y_pos = i as i32 + 1;
        } else {
            break;
        }
    }
    y_pos
}

pub fn part1() -> i64 {
    let input = get_input!().chars().collect::<Vec<_>>();
    let tile_map = generate_tile_map();

    let mut playfield = vec![vec![false; 7]; 10000];
    let mut jet_index = 0;

    for i in 0..2022 {
        let tile = TILE_ORDER[i % 5];

        let mut y_pos = find_max_y(&playfield);
        let mut x_pos = 2;

        y_pos += 4;

        while tile_map.get(&tile).unwrap().iter().all(|(x, y)| {
            y_pos + y > 0 && !playfield[(y_pos + y - 1) as usize][(x_pos + x) as usize]
        }) {
            y_pos -= 1;

            let dir: i32 = match input[jet_index % input.len()] {
                '<' => -1,
                '>' => 1,
                _ => 0,
            };

            if tile_map.get(&tile).unwrap().iter().all(|(x, y)| {
                x + x_pos + dir >= 0
                    && x + x_pos + dir <= 6
                    && !playfield[(y_pos + y) as usize][(x + x_pos + dir) as usize]
            }) {
                x_pos += dir;
            }
            jet_index += 1;
        }
        for (x, y) in tile_map.get(&tile).unwrap().iter() {
            playfield[(y_pos + y) as usize][(x_pos + x) as usize] = true;
        }
    }
    find_max_y(&playfield) as i64
}
pub fn part2() -> i64 {
    let input = get_input!().chars().collect::<Vec<_>>();
    let tile_map = generate_tile_map();

    let mut playfield = vec![vec![false; 7]; 3 * 100_000];
    let mut jet_index = 0;

    let mut height_map = HashMap::new();

    let mut i = 0;
    let mut gamestate;

    loop {
        let tile = TILE_ORDER[i % 5];

        let mut y_pos = find_max_y(&playfield);
        let mut x_pos = 2;

        y_pos += 4;

        while tile_map.get(&tile).unwrap().iter().all(|(x, y)| {
            y_pos + y > 0 && !playfield[(y_pos + y - 1) as usize][(x_pos + x) as usize]
        }) {
            y_pos -= 1;

            let dir: i32 = match input[jet_index % input.len()] {
                '<' => -1,
                '>' => 1,
                _ => 0,
            };

            if tile_map.get(&tile).unwrap().iter().all(|(x, y)| {
                x + x_pos + dir >= 0
                    && x + x_pos + dir <= 6
                    && !playfield[(y_pos + y) as usize][(x + x_pos + dir) as usize]
            }) {
                x_pos += dir;
            }
            jet_index += 1;
        }
        for (x, y) in tile_map.get(&tile).unwrap().iter() {
            playfield[(y_pos + y) as usize][(x_pos + x) as usize] = true;
        }

        let h = y_pos as usize + 3;
        let low = 0.max(h as i32 - 25) as usize;
        gamestate = GameState {
            playfield: playfield[low..h].to_vec(),
            current_tile: tile,
            current_wind_direction: input[jet_index % input.len()],
        };

        if height_map.contains_key(&gamestate) {
            break;
        }
        height_map.insert(gamestate, (i, find_max_y(&playfield)));
        i += 1;
    }
    let (i_new, h_new) = height_map.get(&gamestate).unwrap();
    let hdiff = h_new - find_max_y(&playfield);
    let idiff = i - i_new;
    let loopct = 1_000_000_000 - i_new;
    let rep = loopct / idiff;
    let leftover = loopct % idiff;

    *h_new as i64 + hdiff as i64 * rep as i64 + leftover as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 3117);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), -1553310054);
    }
}
