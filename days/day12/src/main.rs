use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 12;
const DIRECTIONS: [(isize, isize); 4] = [(0, 1), (0, -1), (1, 0), (-1, 0)];

use utils::search_algos;

struct Steps {
    grid: Vec<Vec<u8>>,
    goal: (isize, isize),
}

#[derive(Clone, Hash, Eq, PartialEq)]
struct State {
    value: (isize, isize),
}

impl search_algos::SearchState for State {
    type Global = Steps;
    type Label = (isize, isize);

    fn label(&self) -> Self::Label {
        self.value
    }
    fn is_goal(&self, global: &Self::Global) -> bool {
        self.value == global.goal
    }
    fn successors(&self, global: &Self::Global) -> Vec<(usize, Box<Self>)> {
        let mut result = Vec::new();
        for dir in DIRECTIONS.iter() {
            let next_state = State {
                value: (self.value.0 + dir.0, self.value.1 + dir.1),
            };
            if next_state.value.0 < 0
                || next_state.value.1 < 0
                || next_state.value.0 >= global.grid.len() as isize
                || next_state.value.1 >= global.grid[0].len() as isize
                || global.grid[next_state.value.0 as usize][next_state.value.1 as usize] as i8
                    - (global.grid[self.value.0 as usize][self.value.1 as usize]) as i8
                    > 1
            {
                continue;
            }
            result.push((1, Box::new(next_state)));
        }
        result
    }
}

pub fn part1() -> i64 {
    let input = read_input!("\n", String);
    let mut height_map = vec![];

    // find index of first S

    let mut s_index = (0, 0);
    let mut e_index = (0, 0);

    for (i, line) in input.iter().enumerate() {
        if let Some(x) = line.find('S') {
            s_index = (i as isize, x as isize);
        }
        if let Some(x) = line.find('E') {
            e_index = (i as isize, x as isize);
        }
        height_map.push(
            line.replace('S', "a")
                .replace('E', "z")
                .chars()
                .map(|c| c as u8 - b'a')
                .collect_vec(),
        );
    }

    let steps = Steps {
        grid: height_map,
        goal: e_index,
    };

    let start = State { value: s_index };

    match search_algos::bfs(&steps, &start) {
        Some((cost, _path)) => cost as i64,
        None => panic!("no solution"),
    }
}
pub fn part2() -> i64 {
    let input = read_input!("\n", String);
    let mut height_map = vec![];

    let mut e_index = (0, 0);

    for (i, line) in input.iter().enumerate() {
        if let Some(x) = line.find('E') {
            e_index = (i as isize, x as isize);
        }
        height_map.push(
            line.replace('S', "a")
                .replace('E', "z")
                .chars()
                .map(|c| c as u8 - b'a')
                .collect_vec(),
        );
    }

    let mut possible_starts = vec![];
    for (i, row) in height_map.iter().enumerate() {
        for (j, col) in row.iter().enumerate() {
            if col == &0 {
                possible_starts.push((i as isize, j as isize));
            }
        }
    }

    let steps = Steps {
        grid: height_map,
        goal: e_index,
    };
    // This is a really bad solution but works still fast enough
    possible_starts
        .iter()
        .map(
            |s_index| match search_algos::bfs(&steps, &State { value: *s_index }) {
                Some((cost, _)) => cost as i64,
                None => i64::MAX,
            },
        )
        .min()
        .unwrap()
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 484);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 478);
    }
}
