#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use utils::*;

const CURRENT_DAY: u8 = 11;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Operation {
    MultiplyBy(i64),
    Add(i64),
    Square,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Test {
    divisble_by: i64,
    if_true_target: i64,
    if_false_target: i64,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Monkey {
    id: i64,
    starting_items: Vec<i64>,
    operation: Operation,
    test: Test,
    interactions: i64,
}

impl Monkey {
    pub fn new(id: i64, starting_items: Vec<i64>, operation: Operation, test: Test) -> Self {
        Monkey {
            id,
            starting_items,
            operation,
            test,
            interactions: 0,
        }
    }
}

impl Test {
    pub fn new(divisble_by: i64, if_true_target: i64, if_false_target: i64) -> Self {
        Test {
            divisble_by,
            if_true_target,
            if_false_target,
        }
    }
}

pub fn part1() -> i64 {
    let mut monkeys = parser::MonkeysParser::new().parse(&get_input!()).unwrap();

    for _ in 0..20 {
        for i in 0..monkeys.len() {
            let monkey = monkeys[i].clone();
            for item in monkey.starting_items.iter() {
                let new_item = match monkey.operation {
                    Operation::MultiplyBy(multiplier) => item * multiplier,
                    Operation::Add(addition) => item + addition,
                    Operation::Square => item * item,
                };
                let new_item = new_item / 3;
                if new_item % monkey.test.divisble_by == 0 {
                    monkeys[monkey.test.if_true_target as usize]
                        .starting_items
                        .push(new_item);
                } else {
                    monkeys[monkey.test.if_false_target as usize]
                        .starting_items
                        .push(new_item);
                }
            }
            monkeys[i].interactions += monkey.starting_items.len() as i64;
            monkeys[i].starting_items.clear();
        }
    }
    monkeys.sort_by_key(|monkey| monkey.interactions);
    monkeys.pop().unwrap().interactions * monkeys.pop().unwrap().interactions
}

pub fn part2() -> i64 {
    let mut monkeys = parser::MonkeysParser::new().parse(&get_input!()).unwrap();

    let overall_mod_level: i64 = monkeys
        .iter()
        .map(|monkey| monkey.test.divisble_by)
        .product();

    for _ in 0..10_000 {
        for i in 0..monkeys.len() {
            let monkey = monkeys[i].clone();
            for item in monkey.starting_items.iter() {
                let new_item = match monkey.operation {
                    Operation::MultiplyBy(multiplier) => item * multiplier,
                    Operation::Add(addition) => item + addition,
                    Operation::Square => item * item,
                };
                let new_item = new_item % overall_mod_level;
                if new_item % monkey.test.divisble_by == 0 {
                    monkeys[monkey.test.if_true_target as usize]
                        .starting_items
                        .push(new_item);
                } else {
                    monkeys[monkey.test.if_false_target as usize]
                        .starting_items
                        .push(new_item);
                }
            }
            monkeys[i].interactions += monkey.starting_items.len() as i64;
            monkeys[i].starting_items.clear();
        }
    }
    monkeys.sort_by_key(|monkey| monkey.interactions);
    monkeys.pop().unwrap().interactions * monkeys.pop().unwrap().interactions
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 54253);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 13119526120);
    }
}
