use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 4;

fn parse_tasks(input: Vec<String>) -> Vec<((i64, i64), (i64, i64))> {
    input
        .iter()
        .map(|line| {
            let pair_split = line.split(',').collect::<Vec<_>>();
            let [fst, snd] = pair_split.as_slice() else {panic!("Invalid")};
            let f_split = fst
                .split('-')
                .map(|x| x.parse::<i64>().unwrap())
                .collect::<Vec<_>>();
            let [f0, f1] = f_split.as_slice() else {panic!("Invalid")};
            let s_split = snd
                .split('-')
                .map(|x| x.parse::<i64>().unwrap())
                .collect::<Vec<_>>();

            let [s0, s1] = s_split.as_slice() else {panic!("Invalid")};
            ((*f0, *f1), (*s0, *s1))
        })
        .collect_vec()
}

pub fn part1() -> i64 {
    let input = read_input!("\n", String);

    parse_tasks(input)
        .iter()
        .filter(|((f0, f1), (s0, s1))| (f0 <= s0 && f1 >= s1) || (s0 <= f0 && s1 >= f1))
        .count() as i64
}
pub fn part2() -> i64 {
    let input = read_input!("\n", String);
    parse_tasks(input)
        .iter()
        .filter(|((f0, f1), (s0, s1))| {
            (f0 >= s0 && f0 <= s1)
                || (f1 >= s0 && f1 <= s1)
                || (s0 >= f0 && s0 <= f1)
                || (s1 >= f0 && s1 <= f1)
        })
        .count() as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 466);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 865);
    }
}
