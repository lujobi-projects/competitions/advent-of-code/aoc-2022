#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use std::vec;

use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 10;

#[derive(Debug)]
pub enum Insn {
    Noop,
    AddX(i64),
}

pub fn part1() -> i64 {
    let input = parser::InsnsParser::new().parse(&get_input!()).unwrap();

    let insns = input.iter().flat_map(|i| match i {
        Insn::Noop => vec![Insn::Noop],
        Insn::AddX(x) => vec![Insn::Noop, Insn::AddX(*x)],
    });

    let mut interesting_signals = vec![];
    let mut x = 1;

    for (i, ins) in insns.enumerate() {
        if (i + 1) % 40 == 20 {
            interesting_signals.push(x);
        }
        match ins {
            Insn::Noop => (),
            Insn::AddX(y) => {
                x += y;
            }
        }
    }

    interesting_signals
        .iter()
        .enumerate()
        .map(|(a, b)| (a as i64 * 40 + 20) * b)
        .sum()
}

pub fn part2() -> String {
    let input = parser::InsnsParser::new().parse(&get_input!()).unwrap();

    let insns = input.iter().flat_map(|i| match i {
        Insn::Noop => vec![Insn::Noop],
        Insn::AddX(x) => vec![Insn::Noop, Insn::AddX(*x)],
    });

    let mut res = vec![];
    let mut x = 1;

    for (i, ins) in insns.enumerate() {
        let i = i as i64 % 40;
        if i == x || i == x + 1 || i == x - 1 {
            res.push('#')
        } else {
            res.push('.')
        }
        match ins {
            Insn::Noop => (),
            Insn::AddX(y) => {
                x += y;
            }
        }
    }
    let res = res
        .iter()
        .chunks(40)
        .into_iter()
        .map(|c| c.into_iter().collect::<String>())
        .join("\n");
    format!("\n{}", res)
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 12460);
    }

    #[test]
    fn test_part2() {
        let res = String::from(
            "
####.####.####.###..###...##..#..#.#....
#.......#.#....#..#.#..#.#..#.#.#..#....
###....#..###..#..#.#..#.#..#.##...#....
#.....#...#....###..###..####.#.#..#....
#....#....#....#....#.#..#..#.#.#..#....
####.####.#....#....#..#.#..#.#..#.####.",
        );
        assert_eq!(part2(), res);
    }
}
