use std::{fs::File, io::Read, str::FromStr};

pub fn read_to_string(filepath: &str) -> String {
    let mut file = File::open(filepath).expect("File not found");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Something went wrong reading the file");

    // remove trailing newline
    contents.trim_end().to_string()
}

pub fn read_input(day: u8) -> String {
    let filepath = format!("{}/data/day{}.txt", env!("CARGO_MANIFEST_DIR"), day);
    read_to_string(&filepath)
}

#[allow(dead_code)]
pub fn split_by_chars(content: &str) -> Vec<char> {
    content.chars().collect()
}

#[allow(dead_code)]
pub fn split_separated_1d<T>(content: &str, separator: &str) -> Vec<T>
where
    T: FromStr,
    <T as FromStr>::Err: std::fmt::Debug,
{
    content
        .split(separator)
        .map(|s| s.parse::<T>().unwrap())
        .collect()
}

#[allow(dead_code)]
pub fn split_separated_2d<T>(
    content: &str,
    separator_line: &str,
    separator_column: &str,
) -> Vec<Vec<T>>
where
    T: FromStr,
    <T as FromStr>::Err: std::fmt::Debug,
{
    content
        .split(separator_line)
        .map(|s| {
            s.split(separator_column)
                .map(|s| s.parse::<T>().unwrap())
                .collect()
        })
        .collect()
}

#[macro_export]
macro_rules! print_answer {
    ($day:expr, $part:expr, $answer:expr) => {
        println!("Day {}, Part {}: {}", $day, $part, $answer);
    };
}

#[macro_export]
macro_rules! get_input {
    () => {
        $crate::read_to_string(input_filepath!())
    };
}

#[macro_export]
macro_rules! input_filepath {
    () => {
        format!("{}/input.txt", env!("CARGO_MANIFEST_DIR")).as_str()
    };
}

#[macro_export]
macro_rules! read_input {
    () => {
        $crate::split_by_chars(&read_to_string(input_filepath!()))
    };
    ($sep:expr) => {
        $crate::split_separated_1d(&read_to_string(input_filepath!()), $sep)
    };
    ($sep:expr, $t:ty) => {
        $crate::split_separated_1d::<$t>(&read_to_string(input_filepath!()), $sep)
    };
    ($sep_line:expr, $sep_col:expr) => {
        $crate::split_separated_2d(&read_to_string(input_filepath!()), $sep_line, $sep_col)
    };
    ($sep_line:expr, $sep_col:expr, $t:ty) => {
        $crate::split_separated_2d::<$t>(&read_to_string(input_filepath!()), $sep_line, $sep_col)
    };
}

#[macro_export]
macro_rules! debug_input {
    ($ipt:expr) => {
        $crate::split_by_chars($ipt)
    };
    ($ipt:expr, $sep:expr) => {
        $crate::split_separated_1d($ipt, $sep)
    };
    ($ipt:expr, $sep:expr, $t:ty) => {
        $crate::split_separated_1d::<$t>($ipt, $sep)
    };
    ($ipt:expr, $sep_line:expr, $sep_col:expr) => {
        $crate::split_separated_2d($ipt, $sep_line, $sep_col)
    };
    ($ipt:expr, $sep_line:expr, $sep_col:expr, $t:ty) => {
        $crate::split_separated_2d::<$t>($ipt, $sep_line, $sep_col)
    };
}
