//! Breadth-first search
//!
//! # Example
//!
//! ```rust
//! use utils::bfs;
//!
//! struct Steps {
//!     steps: Vec<(usize, Box<dyn Fn(isize) -> isize>)>,
//!     goal: isize
//! }
//!
//! #[derive(Clone, Hash, Eq, PartialEq)]
//! struct State  {
//!     value: isize,
//! }
//!
//! impl bfs::SearchState for State {
//!     type Global = Steps;
//!     type Label = usize;
//!     
//!     fn label(&self) -> Self::Label {
//!         self.value as usize
//!     }
//!
//!     fn is_goal(&self, global: &Self::Global) -> bool {
//!         self.value == global.goal
//!     }
//!
//!     fn successors(&self, global: &Self::Global) -> Vec<(usize, Box<Self>)> {
//!         let mut result = Vec::new();
//!         for &(cost, ref step) in global.steps.iter() {
//!             let next_state = State{ value: step(self.value) };
//!             result.push((cost, Box::new(next_state)));
//!         };
//!         result
//!     }
//! }
//!
//! let steps = Steps {
//!     steps: vec![
//!         (2, Box::new(|n| { n + 1 })),
//!         (2, Box::new(|n| { n - 1 })),
//!         (3, Box::new(|n| { n * 2 }))],
//!     goal: 23
//! };
//!
//! let start = State { value: 0 };
//!
//! match bfs::bfs(&steps, &start) {
//!     Some((cost, path)) => {
//!         assert_eq!(cost, 17);
//!         assert_eq!(path, vec![0, 1, 2, 3, 6, 12, 24, 23]);
//!     },
//!     None => { panic!("no solution") }
//! }
//! ```

use std::{
    collections::{HashSet, VecDeque},
    fmt::Debug,
    hash::Hash,
};

#[derive(Clone, Debug)]
struct Node<S: SearchState> {
    cost: usize,
    state: S,
    path: Vec<S::Label>,
}

pub trait SearchState {
    type Label: Clone;
    type Global;

    fn label(&self) -> Self::Label;

    fn is_goal(&self, global: &Self::Global) -> bool;

    fn successors(&self, global: &Self::Global) -> Vec<(usize, Box<Self>)>;
}

enum Variant {
    Bfs,
    Dfs,
}

// still a bit weak, needs to run until all cheaper paths have been analyzed
fn search<S>(global: &S::Global, start: &S, variant: Variant) -> Option<(usize, Vec<S::Label>)>
where
    S: Clone + Eq + SearchState + Hash,
{
    let mut queue = VecDeque::new();
    let mut visited = HashSet::new();

    visited.insert(start.clone());
    queue.push_back(Node {
        cost: 0,
        state: start.clone(),
        path: vec![],
    });

    while let Some(v) = match variant {
        Variant::Bfs => queue.pop_front(),
        Variant::Dfs => queue.pop_back(),
    } {
        let mut path = v.path.clone();
        path.push(v.state.label());

        if v.state.is_goal(global) {
            return Some((v.cost, path));
        }

        for succ in v.state.successors(global) {
            if !visited.contains(succ.1.as_ref()) {
                visited.insert(succ.1.as_ref().clone());
                let (n_cost, ref next_neighbor) = succ;
                queue.push_back(Node {
                    cost: n_cost + v.cost,
                    state: next_neighbor.as_ref().clone(),
                    path: path.clone(),
                });
            }
        }
    }
    None
}

pub fn bfs<S>(global: &S::Global, start: &S) -> Option<(usize, Vec<S::Label>)>
where
    S: Clone + Eq + SearchState + Hash,
{
    search(global, start, Variant::Bfs)
}

pub fn dfs<S>(global: &S::Global, start: &S) -> Option<(usize, Vec<S::Label>)>
where
    S: Clone + Eq + SearchState + Hash,
{
    search(global, start, Variant::Dfs)
}
